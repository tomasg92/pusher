#!/usr/bin/env ruby -wKU

class Array
  def mean
    self.inject { |sum, el| sum + el }.to_f/self.size
  end
end

module PusherHelper
  def to_min(seconds)
    separation = seconds.divmod(60)
    [separation[0].to_s + " min",
     separation[1].round(0).to_s + " s"]
  end

  def column(a, b, char = ".")
    """Separate by using column two values"""
    column = char * (COLUMN - a.length - b.to_s.length)
    "#{a} #{column} #{b}"
  end

  def decodify
    @file.each_with_index do |line, index|
      if index == 0
        @name = line
      else
        line = line.sub(/[\n]/,"").split(", ")
        @entries[line[0]] = line[1].to_i
      end
    end
  end
end
