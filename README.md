# Pusher
List processor to get tasks done, measuring quality and time by item.

## Synopsis
`ruby pusher.rb [FILENAME]`

## List syntax
```
LIST NAME
item1, score
.
.
.
itemN, score
```

### Example
```
TODO
Wash dishes, 50
Clean table, 50
Do laundry, 100
```

## Usage
When processed, list items are displayed one by one, waiting for user input, which should a number between 0 and 3

* 0 (bad): task skipped or not done
* 1 (bronce): task done poorly 
* 2 (silver): task done well, but could have been better
* 3 (gold): task done perfectly or as expected

### Example using the TODO list above

```
TODO
Wash dishes ........................... 50
Clean table ........................... 50
Do laundry ........................... 100
 
Wash dishes
3
+150                             7 min 4 s
Clean table
3
+150                             1 min 3 s
Do laundry
2
+200                           36 min 30 s
 
Total	 500 of 600
Grade	 4
Time	 44 min 37 s

```

## Todo
- [ ] Use same numeration system for quality and grade
- [ ] Interruption: get lists done later.
- [ ] Database for non-volatile lists.
- [ ] Categories.