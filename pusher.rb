COLUMN = 40

load "./pusher_helper.rb"

class Pusher
  include PusherHelper
  attr_accessor :entries

  def initialize(file_name)
    @file = File.open(ARGV.first, "r")
    @entries = Hash.new
    @name = nil
    @sum = 0
    @scores = []
  end

  def show
    puts @name.upcase
    @entries.each { |k, v| puts column(k, v) }
    puts " "
  end

  def max_sum
    sum = 0
    @entries.each { |k, v| sum += v }
    sum * 3
  end

  def ask
    start = Time.now
    @entries.each do |k, v|
      puts k
      partial_start = Time.now
      factor = $stdin.gets.chomp.to_i
      @scores.push factor
      partial_end = Time.now
      elapsed = partial_end - partial_start
      puts column("+#{v * factor}", "%s %s" % to_min(elapsed), " ")
      @sum += v * factor
    end
    end_time = Time.now

    puts " "
    puts "Total\t #{@sum} of #{max_sum}"
    average = @scores.mean.round(1)
    puts "Grade\t #{(@sum*5)/max_sum.to_f.round(0)}"
    puts "Time\t %s %s" % to_min(end_time - start)
  end
end

system "clear"
d = Pusher.new ARGV.first
d.decodify
d.show
d.ask
